#!/bin/bash
HTML_TEMPLATE=/opt/iotproject/etc/index_html.template
HTML_OUTPUT_FILE=/var/www/html/index.html
HTML_LIST=/tmp
INTERVALL=1

mosquitto_sub -h localhost -t '#' -F "%t %p" | while read topic payload ;do
    if [ "$topic" == "m5stack-env-III_temperature" ]
    then
        zeit=$(date +"%Y-%m-%d:%H:%M:%S")
        cat $HTML_TEMPLATE | \
            sed "s/_ZEIT_/$zeit/g" | \
            sed "s/_TOPIC_/$topic/g"| \
            sed "s/_TEMP_/$payload/g" > $HTML_OUTPUT_FILE
        sleep $INTERVALL
    fi
done
