#!/bin/bash
HTML_TEMPLATE=/opt/iotproject/etc/index_html.template
HTML_OUTPUT_FILE=/opt/iotproject/index.html
HTML_DATE=$(date +"%Y-%m-%dT%T.%3N%z")
HTML_LIST=/tmp
INTERVALL=2

if [ "$#" -gt 1 ]; 
    then
        if [ -d "$1" ];
            then
                HTML_LIST=$1
        else
            echo "No Directory $1" >&2
        fi
    else
        echo "No Paramater"
fi
shift
counter=0
while [ $counter -le 5 ] ; do

    echo "<html><body>" > $HTML_OUTPUT_FILE
    echo "<h1>Mein Webserver</h1>" >> $HTML_OUTPUT_FILE
    echo $HTML_DATE >> $HTML_OUTPUT_FILE
    ls $HTML_LIST >> $HTML_OUTPUT_FILE 

    for list in $*
    do
        if [ -e "$list" ];
        then
            cat $list >> $HTML_OUTPUT_FILE
        else
            echo "$list is not a file" >&2
        fi
    done
    echo "</body></html>" >> $HTML_OUTPUT_FILE

    sleep $INTERVALL
    ((counter++))

done